package calculator;

import java.util.Scanner;

public class SimpleCalculator {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        boolean status = true;
        while (status) {
            System.out.println("Please Select Arithmetic Operation");
            System.out.println("1:Addition");
            System.out.println("2:Subtraction");
            System.out.println("3:Multiplication");
            System.out.println("4:Division");
            System.out.println("5:Exit");
            int choice = sc1.nextInt();

            if (choice == 1 || choice == 2 || choice == 3 || choice == 4) {
                System.out.println("Enter First Number:");
                int no1 = sc1.nextInt();
                System.out.println("Enter Second Number:");
                int no2 = sc1.nextInt();

                if (choice == 1) {
                    System.out.println("Addition is:" + addition(no1, no2));
                } else if (choice == 2) {
                    System.out.println("Subtraction is:" + subtraction(no1, no2));
                } else if (choice == 3) {
                    System.out.println("Multiplication is:" + multiplication(no1, no2));
                } else if (choice == 4) {
                    System.out.println("Subtraction is:" + division(no1, no2));
                }else {
                    System.out.println("Invalid Choice");
                }
            }else {
                System.out.println("Invalid Choice");
                status = false;
            }
        }
    }

    static int addition(int no1, int no2) {
        return no1 + no2;
    }

    static int subtraction(int no1, int no2) {
        return no1 - no2;
    }

    static int multiplication(int no1, int no2) {
        return no1 * no2;
    }

    static int division(int no1, int no2) {
        return no1 / no2;
    }
}
